<?php 

/**
 *
 */
App::uses('ClassRegistry', 'Utility');
App::uses("AclComponent", "Controller/Component");
App::uses('ComponentCollection', 'Controller');

/**
 *
 */
class UsersSchema extends CakeSchema {

	/**
	 *
	 * @todo $db->cacheSources = false; agregado xq sino no graba lo ultimo
	 */
	public function before($event = array()) {
		$db = ConnectionManager::getDataSource($this->connection);
    	$db->cacheSources = false;
		return true;
	}

	/*
	 *
	 */
	public function after($event = array()) {
		if (isset($event['create'])) {
	        switch ($event['create']) {
				case "acos":
					
					$collection = new ComponentCollection();
	                $acl = new AclComponent($collection);

	                $acl->Aco->create();
	                $node = $acl->Aco->save(['alias' => 'controllers']);
	                $acl->Aco->create();
	                $node = $acl->Aco->save(['alias' => 'Users', 'parent_id' => Hash::get($node, 'Aco.id')]);
	                $acl->Aco->create();
	                $node = $acl->Aco->save(['alias' => 'Users', 'parent_id' => Hash::get($node, 'Aco.id')]);
	                $acl->Aco->create();
	                $acl->Aco->save(['alias' => 'profile', 'parent_id' => Hash::get($node, 'Aco.id')]);
	                $acl->Aco->create();
	                $acl->Aco->save(['alias' => 'logout', 'parent_id' => Hash::get($node, 'Aco.id')]);
	                $acl->Aco->create();
	                $acl->Aco->save(['alias' => 'change_password', 'parent_id' => Hash::get($node, 'Aco.id')]);
	                $acl->Aco->create();
	                $acl->Aco->save(['alias' => 'admin_index', 'parent_id' => Hash::get($node, 'Aco.id')]);
	                $acl->Aco->create();
	                $acl->Aco->save(['alias' => 'admin_add', 'parent_id' => Hash::get($node, 'Aco.id')]);
	                $acl->Aco->create();
	                $acl->Aco->save(['alias' => 'admin_edit', 'parent_id' => Hash::get($node, 'Aco.id')]);
	                $acl->Aco->create();
	                $acl->Aco->save(['alias' => 'admin_view', 'parent_id' => Hash::get($node, 'Aco.id')]);
	                $acl->Aco->create();
	                $acl->Aco->save(['alias' => 'admin_delete', 'parent_id' => Hash::get($node, 'Aco.id')]);
	                $acl->Aco->create();
	                $acl->Aco->save(['alias' => 'admin_active', 'parent_id' => Hash::get($node, 'Aco.id')]);
	                $acl->Aco->create();
	                $acl->Aco->save(['alias' => 'admin_deactive', 'parent_id' => Hash::get($node, 'Aco.id')]);
	                $acl->Aco->create();
	                $acl->Aco->save(['alias' => 'admin_trash', 'parent_id' => Hash::get($node, 'Aco.id')]);
	                $acl->Aco->create();
	                $acl->Aco->save(['alias' => 'admin_restore', 'parent_id' => Hash::get($node, 'Aco.id')]);
	                $acl->Aco->create();
	                $acl->Aco->save(['alias' => 'admin_destroy', 'parent_id' => Hash::get($node, 'Aco.id')]);
					break;

				case "aros_acos": 

					$collection = new ComponentCollection();
	                $acl = new AclComponent($collection);

	                $acl->allow(["model" => "Group", 'foreign_key' => 1], "controllers");

	                $acl->allow(["model" => "Group", 'foreign_key' => 2], "controllers/Users/Users/profile");
	                $acl->allow(["model" => "Group", 'foreign_key' => 2], "controllers/Users/Users/logout");
	                $acl->allow(["model" => "Group", 'foreign_key' => 2], "controllers/Users/Users/change_password");

					break;

	            case "groups":

	                $group = ClassRegistry::init("Users.Group");

	                $group->create();
	                $admin = $group->save(['Group' => ['name' => 'Admin']]);

	                $group->create();
	                $group->save(['Group' => ['name' => 'Users']]);

	                break;
	            case "users":

		            App::uses("SimplePasswordHasher", "Controller/Component/Auth");
					$passwordHasher = new SimplePasswordHasher(['hashType' => 'sha256']);
	                $user = ClassRegistry::init('Users.User');
	                $user->create();
	                $data = ['User' => [
	                	'group_id' => 1,
	                	'username' => Configure::read("Users.admin.username"),
	                	'password' => $passwordHasher->hash(Configure::read("Users.admin.password")),
	                	'active' => true,
	                	'created' => date("Y-m-d H:i:s"),
	                	'modified' => date("Y-m-d H:i:")
	                ]];
	                $user->save($data);
	                break;
	        }
	    }
	    return true;
	}

	/*
	 *
	 */
	public $acos = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => false, 'key' => 'primary'),
		'parent_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => false),
		'model' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'foreign_key' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => false),
		'alias' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'lft' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => false),
		'rght' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	/*
	 *
	 */
	public $aros = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => false, 'key' => 'primary'),
		'parent_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => false),
		'model' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'foreign_key' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => false),
		'alias' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'lft' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => false),
		'rght' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	/*
	 *
	 */
	public $groups = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'key' => 'unique', 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'name' => array('column' => 'name', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => 'table que tiene los roles de usuarios')
	);

	/*
	 *
	 */
	public $users = array(
		'id' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 36, 'key' => 'primary', 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'group_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'username' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'key' => 'unique', 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'password' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'activation_code' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'active' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'deleted' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'username' => array('column' => 'username', 'unique' => 1),
			'group_id' => array('column' => 'group_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => 'tabla de usuarios del sistema')
	);


	/*
	 *
	 */
	public $aros_acos = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => false, 'key' => 'primary'),
		'aro_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => false, 'key' => 'index'),
		'aco_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => false),
		'_create' => array('type' => 'string', 'null' => false, 'default' => '0', 'length' => 2, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'_read' => array('type' => 'string', 'null' => false, 'default' => '0', 'length' => 2, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'_update' => array('type' => 'string', 'null' => false, 'default' => '0', 'length' => 2, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'_delete' => array('type' => 'string', 'null' => false, 'default' => '0', 'length' => 2, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'ARO_ACO_KEY' => array('column' => array('aro_id', 'aco_id'), 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

}