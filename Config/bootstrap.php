<?php

if(!CakePlugin::loaded("Mad")) {
	CakePlugin::load("Mad");
}

App::uses('CakeEventManager', 'Event');
App::uses('UsersListener', 'Users.Event');

$users = new UsersListener();
CakeEventManager::instance()->attach($users);