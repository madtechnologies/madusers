<?php
$users = ['plugin' => 'users', 'controller' => 'users'];
Router::connect("/change-password", Hash::merge($users, ['action' => 'change_password']));
Router::connect("/login", Hash::merge($users, ['action' => 'login']));
Router::connect("/logout", Hash::merge($users, ['action' => 'logout']));
Router::connect("/profile", Hash::merge($users, ['action' => 'profile']));
Router::connect("/register", Hash::merge($users, ['action' => 'register']));
Router::connect("/reset-password", Hash::merge($users, ['action' => 'reset_password']));
