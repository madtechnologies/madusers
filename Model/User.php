<?php

/**
 * Modelo de Users del sistema
 */

/**
 * Uses: 
 * - Users.Model/UsersAppModel
 */
App::uses('CakeEvent', 'Event');
App::uses("UsersAppModel", "Users.Model");

/**
 * Date 2015-10-12
 * @version 0.1
 * @since 0.1
 * @package Users.Model
 * @author Hernan Iglesias <hernan.iglesias@madtechnologies.com.ar>
 */
class User extends UsersAppModel {

	/**
	 * @var $actsAs Array
	 * - Acl
	 */
	public $actsAs = [
		'Acl' => ['type' => 'requester', 'enabled' => false]
	];

	/**
	 * @var $belongsTo Array
	 * - Users.Group
	 */
	public $belongsTo = [
		'Group' => ['className' => 'Users.Group']
	];

	/**
	 * @var array $validate validaciones del modelo.
	 */
	public $validate = [
		'username' => [
			'isUnique' => [
				'rule' => 'isUnique',
				'message' => 'Registro no disponible'
			],
			'notBlank' => [
				'rule' => 'notBlank',
				'message' => 'Registro obligatorio'
			],
			'email' => [
				'rule' => 'email',
				'message' => 'Registro tiene que ser un email'
			]
		]
	];

	public function afterSave($created, $options = []) {
		parent::afterSave($created, $options);
		if($created) {
			$data = $this->read(null, $this->id);
			$event = new CakeEvent("Users.User.afterRegister", $this, ['data' => $data]);
			$this->getEventManager()->dispatch($event);
		}
	}

	/**
	 * Asociacion del Usuario a un grupo para utilizar el Acl Behavior
	 * @param array $user 
	 * @return array 
	 */
	public function bindNode($user) {
		return ['model' => 'Group', 'foreign_key' => Hash::get($user, "User.group_id")];
	}

	/**
	 * Asignacion de Grupo a usuario para utilizar el Acl Behavior
	 * @return array 
	 */
	public function parentNode() {
		if (!$this->id && empty($this->data)) {
			return null;
		}
		$groupId = Hash::get($this->data, "User.group_id");
		if (!$groupId) {
			return null;
		}
		return ['Group' => ['id' => $groupId]];
	}

	/**
	 * Registro global de usuarios y envio de email
	 * @param string $username Email del usuario
	 * @param string $facebookId Id de Facebook
	 * @param string $facebookAccessToken Access Token de Facebook
	 * @return boolean
	 * @throws Exception en el envio de email
	 * @version 0.2
	 * @todo Importante, ver el normalizeEmail para emails que tienen un . en el username
	 */
	public function register($username) {
		$ds = $this->getDataSource();
		$user = $this->createNew($username);
		return $user;
		if($user) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 */
	public function createNew($username) {
		$this->create();
		$this->set("active", false);
		$this->set("group_id", Configure::read("Users.register.groupId"));
		$this->set("username", $username);
		return $this->save();
	}

}	