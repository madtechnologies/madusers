<?php

/**
 * Uses: 
 * - Users.Model/UsersAppModel
 */
App::uses("UsersAppModel", "Users.Model");

/**
 * Modelo de Grupos de Usuario del sistema
 * Date 2015-10-12
 * @version 0.1
 * @since 0.1
 * @package Users.Model
 * @author Hernan Iglesias <hernan.iglesias@madtechnologies.com.ar>
 */
class Group extends UsersAppModel {

	/**
	 * @var $actsAs [Acl]
	 */
	public $actsAs = [
		"Acl" => ['type' => 'requester']
	];

	/**
	 * @var $hasMany [Users.User]
	 */
	public $hasMany = [
		'User' => ['className' => 'Users.User', 'foreignKey' => 'group_id']
	];

	/**
	 * Funcion del Acl para setear que no tiene un registro padre.
	 * @return null
	 */
	public function parentNode() {
		return null;
	}

}