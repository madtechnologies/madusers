<?php

/**
 * Interface de Controller Usuarios con las acciones basicas para su manipulacion
 * Date 2015-10-12
 * @version 0.1
 * @since 0.1
 * @package Users.Controller
 * @author Hernan Iglesias <hernan.iglesias@madtechnologies.com.ar>
 */
interface UsersInterface {

	/**
	 * Activacion de Usuario
	 */
	public function activate($id = null, $code = null);	

	/**
	 * Cambio de Password del Usuario
	 */
	public function change_password();

	/**
	 * Ingreso de Usuario
	 */
	public function login();

	/**
	 * Salida del Usuario
	 */
	public function logout();

	/**
	 * Perfil del Usuario
	 */
	public function profile();

	/**
	 * Registro de Usuario
	 */
	public function register();

	/**
	 * Reseteo de Password del Usuario
	 */
	public function reset_password();

}