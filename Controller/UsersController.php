<?php

/**
 * Uses: 
 * - Network/Email/CakeEmail
 * - Mad.Controller/CrudExtendController
 * - Mad.Controller/Interface/CrudExtendInterface
 * - Mad.Utility/MadString
 * - Users.Controller/UsersAppController
 * - Users.Controller/Interface/UsersInterface
 */
App::uses('CakeEmail', 'Network/Email');
App::uses("CrudExtendController", "Mad.Controller");
App::uses("CrudExtendInterface", "Mad.Controller/Interface");
App::uses("MadString", "Mad.Utility");
App::uses("UsersInterface", "Users.Controller/Interface");

/**
 * Controller de Usuario del Sistema
 * Date 2015-10-12
 * @author Hernan Iglesias <hernan.iglesias@madtechnologies.com.ar>
 * @package Users.Controller
 * @since 0.1
 * @version 0.1
 */
class UsersController extends CrudExtendController implements UsersInterface {

	/**
	 * Activacion de usuarios segun el codigo y el id
	 * @var string $id del Usuario
	 * @var string $activation_code Codigo de activacion
	 */
	public function activate($id = null, $activation_code = null) {
		$user = $this->User->find('first', ['conditions' => ['User.id' => $id, 'User.activation_code' => $activation_code, 'User.active' => false]]);
		if(empty($user)) {
			$this->Session->setFlash("Codigo de Activación Incorrecto o el Usuario ya esta activo");
		} else {
			$ds = $this->User->getDataSource();
			$ds->begin();
			$this->User->id = $id;
			$this->User->set("active", true);
			$user = $this->User->save();
			if($user) {
				$ds->commit();
				$this->Session->setFlash("Usuario activado satisfactoriamente!");
			} else {
				$ds->rollback();
				$this->Session->setFlash("El usuario no pudo activarse. Intente nuevamente.");
			}
		}
		$this->redirect(DS);
	}

	/**
	 * Alta de usuarios desde el admin.
	 */
	public function admin_add() {
		parent::admin_add();
		$groups = $this->User->Group->find('list');
		$this->set('groups', $groups);
	}

	/**
	 * Alta de usuarios desde el admin.
	 */
	public function admin_edit($id = null) {
		parent::admin_edit($id);
		$groups = $this->User->Group->find('list');
		$this->set('groups', $groups);
	}

	/**
	 * Seteo los accesos publicos del controller
	 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Security->requirePost(['reset_password']);
		$this->Security->requirePut(['change_password']);
		$this->Auth->allow(['activate', 'register', 'reset_password']);
	}

	/**
	 * Cambio de contraseña del usuario
	 * Solamente acepta PUT
	 */
	public function change_password() {
		if($this->request->is("put") && !empty($this->request->data)) {
			$user = $this->User->read("password", $this->Auth->user("id"));
			/**
			 * @todo pasar validacion de password al modelo
			 */
			$passwordHasher = new SimplePasswordHasher(['hashType' => 'sha256']);
			$passwordHash = $passwordHasher->hash(Hash::get($this->request->data, "User.password"));
			$password = Hash::get($user, "User.password");
			if($password === $passwordHash) {
				$passwordNew = Hash::get($this->request->data, "User.password_new");
				$passwordNewRepeat = Hash::get($this->request->data, "User.password_new_repeat");
				if($passwordNew === $passwordNewRepeat) {
					if(strlen($passwordNew) >= 8 && strlen($passwordNew) <= 16) {
						$ds = $this->User->getDataSource();
						$ds->begin();
						$this->User->set("password", $passwordHasher->hash($passwordNew));
						$user = $this->User->save();
						if($user) {
							$ds->commit();
							$this->Session->setFlash("Contraseña cambiada exitosamente");
						} else {
							$ds->rollback();
							$this->Session->setFlash("Contraseña no pudo ser cambiada. Intente nuevamente");
						}
					} else {
						$this->Session->setFlash("La contraseña debe tener entre 8 y 16 caracteres");
					}
				} else {
					$this->Session->setFlash("Las contraseñas nuevas no coinciden");
				}
			} else {
				$this->Session->setFlash("La contraseña actual es erronea");
			}
		}
		$this->redirect("/profile");
	}

	/**
	 * Ingreso de usuario
	 */
	public function login() {
		if($this->request->is("post") && !empty($this->request->data)) {
			if($this->Auth->login()) {
				$this->Session->setFlash("Bienvenido!");
				$this->redirect("/profile");
			} else {
				$this->Session->setFlash("Usuario y/o contraseña incorrecto");
			}
		}
	}

	/**
	 * Salida del usuario del sistema
	 */
	public function logout() {
		$this->Auth->logout();
		$this->Session->destroy();
		$this->Session->setFlash("Hasta Pronto!");
		$this->redirect($this->Auth->logoutRedirect);
	}

	/**
	 * Perfil del usuario
	 */
	public function profile() {}

	/**
	 * Registro de usuario
	 */
	public function register() {
		if($this->request->is("post") && !empty($this->request->data)) {
			$user = $this->User->register(Hash::get($this->request->data, "User.username"));
			if($user) {
				$this->Session->setFlash("Chequea tu email y activa tu cuenta!");
				$this->redirect(DS);
			} else {
				$this->Session->setFlash("Error en la creacion del Usuario");
			}
		}
	}

	/**
	 * Reseteo de contraseña
 	 * Solamente acepta POST
	 */
	public function reset_password() {
		if($this->request->is("post") && !empty($this->request->data)) {
			$user = $this->User->find("first", ['conditions' => ['User.username' => Hash::get($this->request->data, 'User.username'), 'User.active' => true]]);
			if(empty($user)) {
				$this->Session->setFlash("El usuario esta inactivo o es inexistente!");
			} else {

				$ds = $this->User->getDataSource();
				$ds->begin();

				$this->User->id = Hash::get($user, "User.id");
				
				$passwordHasher = new SimplePasswordHasher(['hashType' => 'sha256']);
				$password = MadString::stringGenerator();
				$passwordHash = $passwordHasher->hash($password);
				$this->User->set("password", $passwordHash);

				$user = $this->User->save();
				if($user) {
					/**
					 * @todo Chequear de pasar a un evento a un administrador de emails.
					 */
					try {
						$email = new CakeEmail();
						$email->to(Hash::get($this->request->data, "User.username"));
						$email->subject("Reseto de Contraseña en Spondymus");
						$email->emailFormat('html');
						$email->theme('Spondymus');
						$email->template('Users.reset_password', 'default');
						$email->viewVars(array('user' => $user, 'password' => $password));
						$email->send();
					} catch(Exception $e) {
						$ds->rollback();
						throw new Exception($e->getMessage());	
					}

					$ds->commit();
					$this->Session->setFlash("Chequea tu email. Te enviamos una nueva contraseña!");
					$this->redirect(DS);
				} else {
					$ds->rollback();
					$this->Session->setFlash("La contraseña no pudo ser restablecida, intenta mas tarde!");
				}

			}
		}
		$this->redirect(DS);
	}

}