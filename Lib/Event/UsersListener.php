<?php
/**
 * Uses: 
 * - Mad.Utility/MadString
 * - Controller/Component/Auth/SimplePasswordHashser
 * - Users.Model/UsersAppModel
 */
App::uses("CakeEmail", "Network/Email");
App::uses("SimplePasswordHasher", "Controller/Component/Auth");
App::uses("MadString", "Mad.Utility");
App::uses("CakeEventListener", "Event");
App::uses("User", "Users.Model");

/**
 *
 */
class UsersListener implements CakeEventListener {

	/**
	 *
	 */
    public function implementedEvents() {
    	return [
			'Users.User.afterRegister' => array(
                'callable' => 'sendEmailRegistration',
                'passParams' => true
            )
    	];
    }

    /**
	 *
	 */
	public function sendEmailRegistration($data) {

		$user = new User();
		$user->read(null, Hash::get($data, "User.id"));

		$passwordHasher = new SimplePasswordHasher(['hashType' => 'sha256']);
		$password = MadString::stringGenerator();
		$passwordHash = $passwordHasher->hash($password);

		$ds = $user->getDataSource();
		$ds->begin();

		$user->set("password", $passwordHash);
		$user->set("activation_code", MadString::stringGenerator(36));
		
		$data = $user->save();

		if($data) {
			$ds->commit();
			try {
				$email = new CakeEmail();
				$email->to(Hash::get($data, "User.username"));
				$email->subject("Registracion en ".Configure::read("Mad.brandName"));
				$email->emailFormat('html');
				$email->theme(Configure::read("ThemeWeb"));
				$email->template('Users.register', 'default');
				$email->viewVars(array('user' => $data, 'password' => $password));
				$email->send();
			} catch(Exception $e) {
				$ds->rollback();
				throw new Exception($e->getMessage());	
			}
			return true;
		}
		$ds->rollback();
		return false;
    }
}