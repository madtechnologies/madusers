<?php
echo $this->Form->create("User");
echo $this->Form->input("username");
echo $this->Form->input("password");
echo $this->Form->end("Login");
?>
<hr/>
<?php 
echo $this->Form->create("User", ['url' => '/reset-password', 'type' => 'post']);
echo $this->Form->input("username");
echo $this->Form->end("Reset Password");
?>