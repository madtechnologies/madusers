<h2>Usuario: Agregar</h2>
<?php echo $this->Html->btnList()?>

<?php echo $this->Form->create("User");?>
<fieldset>
	<legend>Datos de Usuario</legend>
	<?php
	echo $this->Form->input("User.username");
	echo $this->Form->input("User.active", ["div" => ["class" => "form-class checkbox"]]);
	echo $this->Form->input("User.group_id");
	?>
</fieldset>
<?php
echo $this->Form->end("Guardar");