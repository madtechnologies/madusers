<h2>Usuario: Ver</h2>
<?php echo $this->Html->btnList()?>
<?php echo $this->Html->btnAdd()?>

<dl class="well">
	<dt>Email</dt>
	<?php echo Hash::get($data, "User.username");?>
	<dt>Facebook Id</dt>
	<?php echo Hash::get($data, "User.facebook_id");?>
	<dt>Activo</dt>
	<?php echo Hash::get($data, "User.active");?>
	<dt>Grupo</dt>
	<?php echo Hash::get($data, "Group.name");?>
	<dt>Fecha de Alta</dt>
	<?php echo Hash::get($data, "User.created");?>
	<dt>Fecha de Modificacion</dt>
	<?php echo Hash::get($data, "User.modified");?>
	<dt>Fecha de Baja</dt>
	<?php echo Hash::get($data, "User.deleted");?>
</dl>