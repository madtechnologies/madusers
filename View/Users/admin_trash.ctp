<h2>Usuarios: Listado</h2>
<?php echo $this->Html->btnList()?>
<table class="table">
	<thead>
		<tr>
			<th>
				<?php echo $this->Paginator->sort("Group.name", "Grupo");?>
			</th>
			<th>
				<?php echo $this->Paginator->sort("User.username", "Email");?>
			</th>
			<th>
				<?php echo $this->Paginator->sort("User.facebook_id", "Facebook Id");?>
			</th>
			<th>
				<?php echo $this->Paginator->sort("User.active", "Activo");?>
			</th>
			<th>
				<?php echo $this->Paginator->sort("User.created", "Fecha de Alta");?>
			</th>
			<th>
				<?php echo $this->Paginator->sort("User.created", "Fecha de Modificacion");?>
			</th>
			<th>
				Acciones
			</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($data as $value):?>
		<tr>
			<td>
				<?php echo Hash::get($value, "Group.name")?>
			</td>
			<td>
				<?php echo Hash::get($value, "User.username")?>
			</td>
			<td>
				<?php echo Hash::get($value, "User.facebook_id")?>
			</td>
			<td>
				<?php echo Hash::get($value, "User.active")?>
			</td>
			<td>
				<?php echo Hash::get($value, "User.created")?>
			</td>
			<td>
				<?php echo Hash::get($value, "User.modified")?>
			</td>
			<td class="btn-group">
				<?php echo $this->Form->btn("Restore", 'fa fa-refresh', ["action" => "restore", Hash::get($value, "User.id")], ['class' => 'btn btn-sm btn-warning'])?>
				<?php echo $this->Form->btn("Eliminar", 'fa fa-trash', ["action" => "destroy", Hash::get($value, "User.id")], ['class' => 'btn btn-sm btn-danger'])?>
			</td>
		</tr>
		<?php endforeach?>
	</tobdy>
</table>

<div class="text-center">
	<ul class="pagination">
		<?php echo $this->Paginator->prev('«');?>
		<?php echo $this->Paginator->numbers();?>
		<?php echo $this->Paginator->next('»');?>
	</ul>
</div>	