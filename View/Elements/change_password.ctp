<h2>Change Password</H2>
<?php 
echo $this->Form->create("User", ['url' => '/change-password', 'type' => 'put']);
echo $this->Form->input("password");
echo $this->Form->input("password_new", ['type' => 'password']);
echo $this->Form->input("password_new_repeat", ['type' => 'password']);
echo $this->Form->end("Change Password");
?>