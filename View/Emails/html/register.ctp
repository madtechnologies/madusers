<h1>Registracion en <?php echo Configure::read("Mad.brandName")?></h1>
<dl>
	<dt>Tu Contraseña es:</dt>
	<dd><?php echo $password?></dd>
	<dt>Para activar tu cuenta hace click en el siguiente enlace:</dt>
	<dd>
		<?php 
		$activationCode = Hash::get($user, "User.activation_code");
		$id = Hash::get($user, "User.id");
		echo $this->Html->link("Activar Cuenta", ['plugin' => 'users', 'controller' => 'users', 'action' => 'activate', 'prefix' => false, 'admin' => false, $id, $activationCode, 'full_base' => true]);
		?>
	</dd>
</dl>